<?php
// "Hãy viết function findProductByPrice(listProduct, price) 
// trả về danh sách tên product có giá <= price truyền vào"
$listProduct = array(
   array('name' => 'CPU', 'price' => 750, 'quality' => 10, 'categoryId' => 1),
   array('name' => 'RAM', 'price' => 50, 'quality' => 2, 'categoryId' => 2),
   array('name' => 'HDD', 'price' => 70, 'quality' => 1, 'categoryId' => 2),
   array('name' => 'Main', 'price' => 400, 'quality' => 3, 'categoryId' => 1),
   array('name' => 'Keyboard', 'price' => 30, 'quality' => 8, 'categoryId' => 4),
   array('name' => 'Mouse', 'price' => 25, 'quality' => 50, 'categoryId' => 4),
   array('name' => 'VGA', 'price' => 60, 'quality' => 35, 'categoryId' => 3),
   array('name' => 'Monitor', 'price' => 120, 'quality' => 28, 'categoryId' => 2),
   array('name' => 'Case', 'price' => 120, 'quality' => 28, 'categoryId' => 5)
);

function findProductByPrice($listProduct, $price)
{
   $listItem = array();
   foreach ($listProduct as $key => $value) {
      if ($price <= $listProduct[$key]['price']) {
         $listItem[] = $listProduct[$key];
      }
   }
   return $listItem;
}

$result = findProductByPrice($listProduct, '300');
echo '<pre>';
print_r($result);
echo '</pre>';
