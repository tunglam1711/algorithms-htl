Mảng (Array) là một trong các cấu trúc dữ liệu quan trọng nhất. 
Mảng có thể lưu giữ một số phần tử cố định và các phần tử này nên có cùng kiểu. Hầu hết các cấu trúc dữ liệu đều sử dụng mảng để triển khai giải thuật.
Trong mảng có các khái niệm quan trọng là phần tử(được lưu giữ trong mảng) và chỉ mục(index-mỗi vị trí trong mảng có chỉ mục số dùng để nhận diện phần tử)
Mảng gồm các bản ghi có kiểu giống nhau, có kích thước cố định, mỗi phần tử được xác định bởi chỉ số, được cấp phát liên tục.
Trong mảng có các hoạt động cơ bản như duyêt, chèn,xoá, tìm kiếm, cập nhật
