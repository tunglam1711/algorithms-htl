<?php 

$listProduct = array(
   array('name' => 'CPU', 'price' => 750, 'quality' => 10, 'categoryId' => 1),
   array('name' => 'RAM', 'price' => 50, 'quality' => 2, 'categoryId' => 2),
   array('name' => 'HDD', 'price' => 70, 'quality' => 1, 'categoryId' => 2),
   array('name' => 'Main', 'price' => 400, 'quality' => 3, 'categoryId' => 1),
   array('name' => 'Keyboard', 'price' => 30, 'quality' => 8, 'categoryId' => 4),
   array('name' => 'Mouse', 'price' => 25, 'quality' => 50, 'categoryId' => 4),
   array('name' => 'VGA', 'price' => 60, 'quality' => 35, 'categoryId' => 3),
   array('name' => 'Monitor', 'price' => 120, 'quality' => 28, 'categoryId' => 2),
   array('name' => 'Case', 'price' => 120, 'quality' => 28, 'categoryId' => 5)
);

$listCategory = array(
   array('id' => 1, 'name' => 'Computer'),
   array('id' => 2, 'name' => 'Memory'),
   array('id' => 3, 'name' => 'Card'),
   array('id' => 4, 'name' => 'Accessory')
);

function mapProductByCategory($listProduct, $listCategory){
   $quantityProduct = count($listProduct);
   $quantityCategory = count($listCategory);

   for($i = 0; $i <$quantityProduct; $i++){
      for($j = 0; $j<$quantityCategory; $j++){
         if($listCategory[$j]['id']==$listProduct[$i]['categoryId']){
            $listProduct[$i]['categoryId']=$listCategory[$j]['name'];
            break;
         }
      }
   }
   return $listProduct;
} 
?>