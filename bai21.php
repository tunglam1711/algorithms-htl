<?php

//Recursive
function calSalaryRecursive($salary, $n)
{
   $rate = 0.1;
   if ($n == 0) {
      return $salary;
   } else {
      return calSalaryRecursive($salary, $n - 1) * (1 + $rate);
   }
}

//Non-recursive
function calSalaryNonRecursive($salary, $n)
{
   $newSalary = $salary;
   $rate = 0.1;
   for ($i = 1; $i <= $n; $i++) {
      $newSalary = $newSalary*(1 + $rate);
   }
   return $newSalary;
}
