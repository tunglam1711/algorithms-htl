<?php

//Non-recursive
function calMonthNonRecursive($money, $rate)
{
   $balance = $money;
   $months = 0;
   while ($balance < $money * 2) {
      $interest = $balance * $rate / 100;
      $balance += $interest;
      $months++;
   }
   return $months;
}

//Recursive
function calMonthRecursive($money, $rate, $months = 0, $newMoney = 0)
{
   if ($newMoney == 0) {
      $newMoney = $money * 2;
   }

   $interest = $money * $rate / 100;
   $newMoney += $interest;
   if ($newMoney >= $money * 2) {
      return $months;
   }
   return calMonthRecursive($newMoney, $rate, $months + 1, $newMoney);
}
