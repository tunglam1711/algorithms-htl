<?php
require_once './bai14.php';


function sortByCategoryName($listProduct, $listCategory)
{
   $quantityProduct = count($listProduct);

   $productByCategory = mapProductByCategory($listProduct, $listCategory);

   for ($i = 1; $i < $quantityProduct; $i++) {
      $j = $i;
      while ($j > 0 && strcmp($productByCategory[$j - 1]['categoryId'], $productByCategory[$j]['categoryId']) > 0) {
         $temp = $productByCategory[$j];
         $productByCategory[$j] = $productByCategory[$j - 1];
         $productByCategory[$j - 1] = $temp;
         $j--;
      }
   }
   return $productByCategory;
}
